# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#' Retrieves data from the similarweb endpoint "Desktop vs. Mobile Share".
#' Similarweb only delivers the most recent data.
#' Documentation: https://www.similarweb.com/corp/developer/desktop_and_mobile_web
#'
#' @param domain string e.g. "adblockplus.org" (Required).
#' @param api_key similarweb API key (Required).
#' @param country ISO Alpha-2 country code, defaults to "world"
#' @param main_domain_only Defaults to "false", not sure what this corresponds to,
#' since it is not documented anywhere, but values returned with true and false
#' differ from each other. Documentation can be found here:
#' https://www.similarweb.com/corp/developer/desktop_and_mobile_web
#' @keywords similarweb, desktop traffic, mobile traffic
#' @import dplyr
#' @importFrom purrr insistently rate_backoff
#' @importFrom httr GET content
#' @importFrom lubridate day<-

#' @export
sw_desktop_mobile <- function(domain,
                              api_key,
                              country = "world",
                              main_domain_only = "false")
  {
  # for our plan, it seems we can't input anything else but monthly granularity
  # otherwise this part would require some sort of logic to differentiate
  data <- insistently(GET,
                      rate = rate_backoff(max_times = 7),
                      quiet = FALSE)(
                        url = paste0(
                          "https://api.similarweb.com/v1/website/",
                          domain,
                          "/total-traffic-and-engagement/visits-split"
                        ),
                        query = list(
                          api_key = api_key,
                          country = country,
                          main_domain_only = main_domain_only
                        )
                      ) %>%
    content()

  if(is.null(data$meta$error_code)) {
    date <- as.Date(data$meta$last_updated)
    day(date) <- 1

    result <- tibble(
      desktop_visit_share = ifelse(is.null(data$desktop_visit_share), NA, data$desktop_visit_share),
      mobile_web_visit_share = ifelse(is.null(data$mobile_web_visit_share), NA, data$mobile_web_visit_share)
    ) %>%
      mutate(
        date = date,
        domain = domain,
        country = country
      )
  } else {
    cat("No data found for sw_visits ", domain, "\n")
    result <- tibble(
      domain = character(0),
      country = character(0),
      date = structure(numeric(0), class = "Date"),
      desktop_visit_share = numeric(0),
      mobile_web_visit_share = numeric(0)
    )
  }

  return(result)

}
