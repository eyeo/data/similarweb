#' Retrieves data from the similarweb endpoint "Visits".
#'
#' @param date String in the format "YYYY-MM-DD", can go back up to 6 months (Required).
#' @param api_key similarweb API key
#' @param country ISO Alpha-2 country code, defaults to "world"
#' @param device String either total, desktop or mobile
#' @param domain URL string e.g. "adblockplus.org" (Required)
#' @param months Integer Amount of months you want to query from the API, default = 1, maximal 6 possible
#' @param granularity Set the granularity for the returned values. Can be 'daily', 'weekly' or 'monthly'.
#' @param main_domain_only Return values for the main domain only ('true'), or include also the subdomains ('false')
#' @keywords similarweb, visits
#' @import dplyr
#' @importFrom lubridate day<- month month<-
#' @importFrom purrr insistently rate_backoff map_df
#' @importFrom httr GET content

#' @export
sw_visits <- function(date,
                      api_key,
                      country = "world",
                      device = "total",
                      domain,
                      months = 1,
                      granularity = "monthly",
                      main_domain_only = "false"){

  # check country input
  if (str_length(country) < 2) {
    stop("The country code has to be at least two characters long.\n")
  }

  #Date handling
  if(months > 6) {
    cat("You cannot have more than 6 month of data from the similarweb API\n")
    months <- 6
    cat("Running with months = 6 instead\n")
  }

  last_available_date <- Sys.Date()
  day(last_available_date) <- 1
  month(last_available_date) <- month(last_available_date) - 6 + 1

  end_date <- as.Date(date)
  day(end_date) <- 1

  start_date <- end_date
  month(start_date) <- month(start_date) - months + 1

  if (start_date < last_available_date) {
    start_date <- last_available_date
  }

  if (end_date < last_available_date) {
    end_date <- last_available_date
  }

  endpoint <- case_when(
    device == "total" ~ "/total-traffic-and-engagement/visits",
    device == "desktop" ~ "/traffic-and-engagement/visits",
    device == "mobile" ~ "/mobile-web/visits"
  )

  version <- case_when(device == "total" ~ "v1",
                       device == "desktop" ~ "v1",
                       device == "mobile" ~ "v2")

  data <-  insistently(GET,
                       rate = rate_backoff(max_times = 7),
                       quiet = FALSE)(
                         url = paste0(
                           "https://api.similarweb.com/",
                           version,
                           "/website/",
                           domain,
                           endpoint
                         ),
                         query = list(
                           api_key = api_key,
                           start_date = start_date,
                           end_date = end_date,
                           country = country,
                           main_domain_only = main_domain_only,
                           granularity = granularity
                         )
                       ) %>%
    content()

  to_df <- function(list){
    if(is.null(list$visits)) {
      return(tibble(date = list$date, visits = NA))
    }
    return(tibble(date = list$date, visits = list$visits))
  }

  if(is.null(data$meta$error_code)) {
    result <- data$visits %>%
      map_df(to_df) %>%
      mutate(
        date = as.Date(date),
        domain = domain,
        device = device,
        country = country
      )
  } else {
    cat("No data found for sw_visits ", domain, "\n")
    result <- tibble(
      domain = character(0),
      device = character(0),
      country = character(0),
      date = structure(numeric(0), class = "Date"),
      visits = numeric(0)
    )
  }

  return(result)
}
