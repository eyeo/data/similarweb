# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#' Cleans data obtained from sw_api_lite and puts it into long format.
#' @param sw_data Data returned by sw_api_lite in JSON format (Required)
#' @keywords similarweb, general data, traffic
#' @import dplyr tidyr

#' @export
sw_clean_lite <- function(sw_data) {

  # remove redundant data and stuff there's really no chance we will need ever
  sw_data$large_screenshot <- NULL
  sw_data$top_social$icon <- NULL
  sw_data$similar_sites$screenshot <- NULL
  sw_data$similar_sites_by_rank <- NULL

  # the mobile apps data is interesting but we will work on it separately
  sw_data$mobile_apps <- NULL

  # the estimated_monthly_visits needs a little unpacking
  sw_data$estimated_monthly_visits <- list(
    "date"  = names(sw_data$estimated_monthly_visits),
    "value" = unlist(sw_data$estimated_monthly_visits) %>% setNames(NULL)
  )

  # everything is now ready to be put in long format!
  sw_df <- sw_data %>%
    unlist() %>%
    t() %>%
    as.data.frame() %>%
    # TODO it seems more data has been added in the meantime and/or the format changed.
    # TODO this technically works, but adds a .copy variable due to duplicated column names.
    pivot_longer(-site_name, names_to = "key", values_to = "value")

  return(sw_df)

}
