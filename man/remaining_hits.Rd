% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/remaining_hits.R
\name{remaining_hits}
\alias{remaining_hits}
\title{Retrieves number of remaining hits in your plan. For monthly plans,
this data resets every month.}
\usage{
remaining_hits(api_key)
}
\arguments{
\item{api_key}{similarweb API key (Required)}
}
\description{
Retrieves number of remaining hits in your plan. For monthly plans,
this data resets every month.
}
\keyword{hits}
\keyword{remaining}
\keyword{similarweb,}
